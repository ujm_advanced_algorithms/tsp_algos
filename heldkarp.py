import itertools
import sys

import parser.tsp_parser as tsp_parser

def held_karp(dists):
    """
    Implementation of Held-Karp, an algorithm that solves the Traveling
    Salesman Problem using dynamic programming with memoization.
    Parameters:
        dists: distance matrix
    Returns:
        A tuple, (cost, path).
    """
    n = len(dists)

    # Maps each subset of the nodes to the cost to reach that subset, as well
    # as what node it passed before reaching this subset.
    # Node subsets are represented as set bits.
    C = {}

    # Set transition cost from initial state
    for k in range(1, n):
        C[(1 << k, k)] = (dists[0][k], 0)

    # Iterate subsets of increasing length and store intermediate results
    # in classic dynamic programming manner
    for subset_size in range(2, n):
        for subset in itertools.combinations(range(1, n), subset_size):
            # Set bits for all nodes in this subset
            bits = 0
            for bit in subset:
                bits |= 1 << bit

            # Find the lowest cost to get to this subset
            for k in subset:
                prev = bits & ~(1 << k)

                res = []
                for m in subset:
                    if m == 0 or m == k:
                        continue
                    res.append((C[(prev, m)][0] + dists[m][k], m))
                C[(bits, k)] = min(res)

    # We're interested in all bits but the least significant (the start state)
    bits = (2**n - 1) - 1

    # Calculate optimal cost
    res = []
    for k in range(1, n):
        res.append((C[(bits, k)][0] + dists[k][0], k))
    opt, parent = min(res)

    # Backtrack to find full path
    path = []
    for i in range(n - 1):
        path.append(parent)
        new_bits = bits & ~(1 << parent)
        _, parent = C[(bits, parent)]
        bits = new_bits

    # Add implicit start state
    path.append(0)

    return list(reversed(path)), opt


if __name__ == '__main__':
    print('---------------------------------------')
    print('--------Held Karp algorithm------------')
    print('---------------------------------------')
    arg = ''

    try:
        arg = sys.argv[1]
    except:
        pass

    if arg is None or arg == '':
        arg = 'ulysses16.tsp'


    data_parsed = None


    try:
        data_parsed = tsp_parser.parse_data(arg)
    except:
        print ('tsp_heldkarp - Fatal error!')
        print ('  Could not parse this file. Please check that your file is well formatted')
        raise Exception('tsp_heldkarp - Fatal error!')

    dimensions, dists = data_parsed

    rows_count = dimensions[0]
    columns_count = dimensions[1]
    if rows_count != columns_count:
        print ('tsp_heldkarp - Fatal error!')
        print ('  The distance matrix must be square.')
        print ('  Your matrix has M = %d, N = %d' % (rows_count, columns_count))
        raise Exception('tsp_heldkarp - Fatal error!')

    # Pretty-print the distance matrix
    print ('')
    print ('  The city-to-city distance matrix:')
    print ('')
    for row in dists:
        print(' '.join([str(n).rjust(3, ' ') for n in row]))

    print('')

    best_permutation, best_distance = held_karp(dists)
    print("Minimum cost :", best_distance)
    print("Path Taken : ", end=' ')

    for i in range(columns_count):
        print(best_permutation[i], end=' ')

    print('')
