#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re
import numpy as np

"""
Putting it all together
"""
def read_tsp_data(tsp_name):
    tsp_name = 'data/'+tsp_name
    with open(tsp_name) as f:
        content = f.read().splitlines()
        cleaned = [x.lstrip() for x in content if x != ""]
        return cleaned

def detect_dimension(in_list):
    non_numeric = re.compile(r'[^\d]+')
    for element in in_list:
        if element.startswith("DIMENSION"):
            return non_numeric.sub("", element)

def get_cities(list,dimension):
    cities_set = []
    dimension = int(dimension)
    for item in list:
        for num in range(1, dimension + 1):
            if item.startswith(str(num)):
                index, space, rest = item.partition(' ')
                if rest not in cities_set:
                    cities_set.append(rest)
    return cities_set

def city_tup(list):
    cities_tups = []
    for item in list:
        array_partition = item.split(' ')
        current_sets = []
        for it in array_partition:
            try:
                it = int(it)
            except:
                it = float(it)

            if it is not None:
                current_sets.append(it)

        cities_tups.append(current_sets)

    return cities_tups

def parse_data(tsp_name):
    data = read_tsp_data(tsp_name)
    dimension = detect_dimension(data)
    cities_set = get_cities(data, dimension)
    cities_tups = city_tup(cities_set)

    distances = np.array(cities_tups)
    dimensions = np.shape(distances)

    return dimensions, distances

