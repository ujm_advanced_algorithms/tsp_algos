# Python3 program to solve
# Traveling Salesman Problem using
# Minimum Spanning tree.
import math
import sys
import parser.tsp_parser as tsp_parser


class Graph:

    def __init__(self, vertices):
        self.V = vertices
        self.graph = [[0 for column in range(vertices)]
                      for row in range(vertices)]
        print(self.graph)
    # A utility function to print the constructed MST stored in parent[]
    def printMST(self, parent):
        print ("Edge \tWeight")
        for i in range(1, self.V):
            print(parent[i], "-", i, "\t", self.graph[i][parent[i]])

    # A utility function to find the vertex with
    # minimum distance value, from the set of vertices
    # not yet included in shortest path tree
    def minKey(self, key, mstSet):

        # Initialize min value
        min = sys.maxsize

        for v in range(self.V):
            if key[v] < min and mstSet[v] == False:
                min = key[v]
                min_index = v

        return min_index

    # Function to construct and print MST for a graph
    # represented using adjacency matrix representation
    def primMST(self):

        # Key values used to pick minimum weight edge in cut
        key = [sys.maxsize] * self.V
        parent = [None] * self.V  # Array to store constructed MST
        # Make key 0 so that this vertex is picked as first vertex
        key[0] = 0
        mstSet = [False] * self.V

        parent[0] = -1  # First node is always the root of

        for cout in range(self.V):

            # Pick the minimum distance vertex from
            # the set of vertices not yet processed.
            # u is always equal to src in first iteration
            u = self.minKey(key, mstSet)

            # Put the minimum distance vertex in
            # the shortest path tree
            mstSet[u] = True

            # Update dist value of the adjacent vertices
            # of the picked vertex only if the current
            # distance is greater than new distance and
            # the vertex in not in the shortest path tree
            for v in range(self.V):

                # graph[u][v] is non zero only for adjacent vertices of m
                # mstSet[v] is false for vertices not yet included in MST
                # Update the key only if graph[u][v] is smaller than key[v]
                if self.graph[u][v] > 0 and mstSet[v] == False and key[v] > self.graph[u][v]:
                    key[v] = self.graph[u][v]
                    parent[v] = u

        self.printMST(parent)



# Driver code
columns_count = 0
rows_count = 0
if __name__ == '__main__':
    print('---------------------------------------')
    print('--------Minimum Spanning tree algorithm--------')
    print('---------------------------------------')

    arg = ''

    try:
        arg = sys.argv[1]
    except:
        pass

    if arg is None or arg == '':
        arg = 'ulysses16.tsp'


    data_parsed = None

    try:
        data_parsed = tsp_parser.parse_data(arg)
    except:
        print ('tsp_minimum_spanning_tree - Fatal error!')
        print ('  Could not parse this file. Please check that your file is well formatted')
        raise Exception('tsp_minimum_spanning_tree - Fatal error!')

    dimensions, dists = data_parsed
    rows_count = dimensions[0]
    columns_count = dimensions[1]
    if rows_count != columns_count:
        print('tsp_minimum_spanning_tree - Fatal error!')
        print('  The distance matrix must be square.')
        print('  Your matrix has M = %d, N = %d' % (rows_count, columns_count))
        raise Exception('tsp_minimum_spanning_tree - Fatal error!')

    # Pretty-print the distance matrix
    print('')
    print('  The city-to-city distance matrix:')
    print('')
    for row in dists:
        print(' '.join([str(n).rjust(3, ' ') for n in row]))

    print('')
    g = Graph(columns_count)
    g.graph = dists
    g.primMST()





