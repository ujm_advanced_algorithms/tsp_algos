This project is an implementation of the different algorithms used to solve the Travelling Salesman Problem.


1- The brute-force approach exploring all the possible solutions in a systematic way.</br>
2- A Branch-and-Bound version.</br>
3- The approximation based on the minimum spanning tree as seen in the exercise sheet.</br>
4- A version considering at each step the nearest unvisited choice (called the greedy approach).</br>
5- A dynamic programming approach, you take inspiration from the paper of Bouman et al. uploaded on claroline 1, and from the wikipedia page of the Held-Karp algorithm: https: //en.wikipedia.org/wiki/Held%E2%80%93Karp_algorithm.</br>
6- The Branch-and-Bound algorithm based on edge selection and matrix reduction presented in the file TSP BB-edges-Reingold77.pdf2</br>
7- A version based on a randomized approach.</br>
8- A version based on a genetic programming or ant colony approach.</br>
9- Another personal version of you choice</br>



# Project organization
- We have a folder called <data> :
Inside this folder we have some examples of tsp data downloaded from tsp databases</br>
- We have a folder called <parser> :
Inside this folder we have the file tsp_parser.py that is used by all algorithms to parse any tsp file</br>
- The algorithms files are directly put in the folder</br>



## Project requirements
```
You should have installed python 3. 
```

## Run project
```
You can can run one the python file by the following
$ python <python file> <tsp file>
or
$ python3 <python file> <tsp file>

The python or python3 using depends on your python version installed.
For example python3 is avalaible for the keyword "python3"

Eg:
$ python3 heldkarp.py att532.tsp


```


### Pictures of results
```
You can see it in the folder "demos"
```
Professor: Amaury Habrard <br/>
Authors: 
Soumya Kumbar
Abdiwahab Rabileh
Hodonou Hinnougnon Emmanuel
Shreyas Harinath
