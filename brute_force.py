from itertools import permutations
from typing import Any, List, Optional, Tuple
import numpy as np
import sys
import parser.tsp_parser as tsp_parser

"""Auxiliar functions that may be used in most modules"""
from typing import List

def compute_permutation_distance(
    distance_matrix: np.ndarray, permutation: List[int]
) -> float:
    """Compute the total route distance of a given permutation

    Parameters
    ----------
    distance_matrix
        Distance matrix of shape (n x n) with the (i, j) entry indicating the
        distance from node i to j. It does not need to be symmetric

    permutation
        A list with nodes from 0 to n - 1 in any order

    Returns
    -------
    Total distance of the path given in ``permutation`` for the provided
    ``distance_matrix``

    Notes
    -----
    Suppose the permutation [0, 1, 2, 3], with four nodes. The total distance
    of this path will be from 0 to 1, 1 to 2, 2 to 3, and 3 back to 0. This
    can be fetched from a distance matrix using:

        distance_matrix[ind1, ind2], where
        ind1 = [0, 1, 2, 3]  # the FROM nodes
        ind2 = [1, 2, 3, 0]  # the TO nodes

    This can easily be generalized to any permutation by using ind1 as the
    given permutation, and moving the first node to the end to generate ind2.
    """
    ind1 = permutation
    ind2 = permutation[1:] + permutation[:1]
    return distance_matrix[ind1, ind2].sum()

def solve_tsp_brute_force(
    distance_matrix: np.ndarray,
) -> Tuple[Optional[List], Any]:
    """Solve TSP to optimality with a brute force approach

    Parameters
    ----------
    distance_matrix
        Distance matrix of shape (n x n) with the (i, j) entry indicating the
        distance from node i to j. It does not need to be symmetric

    Returns
    -------
    A permutation of nodes from 0 to n that produces the least total
    distance

    The total distance the optimal permutation produces

    Notes
    ----
    The algorithm checks all permutations and returns the one with smallest
    distance. In principle, the total number of possibilities would be n! for
    n nodes. However, we can fix node 0 and permutate only the remaining,
    reducing the possibilities to (n - 1)!.
    """

    # Exclude 0 from the range since it is fixed as starting point
    points = range(1, distance_matrix.shape[0])
    best_distance = np.inf
    best_permutation = None
    for partial_permutation in permutations(points):
        # Remember to add the starting node before evaluating it
        permutation = [0] + list(partial_permutation)
        distance = compute_permutation_distance(distance_matrix, permutation)
        if distance < best_distance:
            best_distance = distance
            best_permutation = permutation

    return best_permutation, best_distance

if __name__ == '__main__':
    print('---------------------------------------')
    print('--------Brute force algorithm----------')
    print('---------------------------------------')
    arg = ''

    try:
        arg = sys.argv[1]
    except:
        pass

    if arg is None or arg == '':
        arg = 'ulysses16.tsp'


    data_parsed = None


    try:
        data_parsed = tsp_parser.parse_data(arg)
    except:
        print ('tsp_brute - Fatal error!')
        print ('  Could not parse this file. Please check that your file is well formatted')
        raise Exception('tsp_brute - Fatal error!')

    dimensions, dists = data_parsed

    rows_count = dimensions[0]
    columns_count = dimensions[1]
    if rows_count != columns_count:
        print ('tsp_brute - Fatal error!')
        print ('  The distance matrix must be square.')
        print ('  Your matrix has M = %d, N = %d' % (rows_count, columns_count))
        raise Exception('tsp_brute - Fatal error!')

    # Pretty-print the distance matrix
    print ('')
    print ('  The city-to-city distance matrix:')
    print ('')
    for row in dists:
        print(' '.join([str(n).rjust(3, ' ') for n in row]))

    print('')

    best_permutation, best_distance = solve_tsp_brute_force(dists)
    print("Minimum cost :", best_distance)
    print("Path Taken : ", end=' ')

    for i in range(columns_count ):
        print(best_permutation[i], end=' ')

    print('')



